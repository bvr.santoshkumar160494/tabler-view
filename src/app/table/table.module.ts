import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TableComponent } from './table.component';
import { FilterPipe } from '../shared/filter.pipe';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';
import { AddComponent } from './add/add.component';

const routes: Routes = [
  {
    path: '',
    component: TableComponent
  },
  {
    path: 'operations',
    component: AddComponent
  }
];


@NgModule({
  declarations: [TableComponent,FilterPipe,AddComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [FilterPipe],
})
export class TableModule { }
