import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SubjectService } from 'src/app/shared/subject.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  data : any;
  displayName : string = '';
  employeeForm:FormGroup;
  id : any;
  constructor(public service : SubjectService,public router : Router) { 
    this.employeeForm = new FormGroup({
          'name':new FormControl('',[Validators.required,Validators.min(4)]),
          'phone':new FormControl('',[Validators.required, Validators.min(1000000000), Validators.max(99999999999)]),
         'address' : new FormGroup({
          'city':new FormControl('',[Validators.required]),
          'address_line1':new FormControl('',[Validators.required]),
          'postal_code':new FormControl('',[Validators.required]),
          })
    });
    this.service.editData.subscribe(value=>{
      if(value){
        this.displayName = value.type;
        this.id = value.id -1
        console.log(this.displayName)
        let newData = {
           name : value['name'],
           phone : value['phone'],
           address : {
            city : value['address']['city'],
            address_line1 : value['address']['address_line1'],
            postal_code : value['address']['postal_code'],
           }
        }
        this.data = value;
        this.employeeForm.setValue(newData)
      }
   })
    this.service.dataTranster.subscribe(value=>{
      if(Object.keys(value).length !== 0){
        this.employeeForm.reset()
        this.data = value;
        this.displayName = this.data.type
      }
   })
  }

  onSubmit(){
    console.log(this.employeeForm.value)
    let formData = this.employeeForm.value
    console.log(formData)
    formData['type'] = this.displayName;
    formData['id'] = this.id+1;
    formData['key'] = formData['id']-1;
    this.service.formdata.next(formData);
    this.router.navigateByUrl('/')
  }

  ngOnInit(): void {
      
  }

}
