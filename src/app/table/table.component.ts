import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SubjectService } from '../shared/subject.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  searchText: string='';
  dataList = [{
    "id": 1,
    "name": "Jhon",
    "phone": "9999999999",
    "address": {
      "city": "Pune",
      "address_line1": "ABC road",
      "address_line2": "XYZ building",
      "postal_code": "12455"
    }
  }, {

    "id": 2,
    "name": "Jacob",
    "phone": "AZ99A99PQ9",
    "address": {
      "city": "Pune",
      "address_line1": "PQR road",
      "address_line2": "ABC building",
      "postal_code": "13455"
    }
  }, {
    "id": 3,
    "name": "Ari",
    "phone": "145458522",
    "address": {
      "city": "Mumbai",
      "address_line1": "ABC road",
      "address_line2": "XYZ building",
      "postal_code": "12455"
    }
  }]

  constructor(public router : Router,public service : SubjectService) { 
    this.service.formdata.subscribe(value=>{
      console.log("data",value)
      if(Object.keys(value).length !== 0){
        if(value.type === "Edit Employee"){
          this.dataList[value.key] = value;
        } else {
          value['id'] = this.dataList.length+1
          this.dataList.push(value);
        }
      }
   })
  }

  ngOnInit(): void {
    JSON.stringify(this.dataList);
  }

  getNum(value:any) {
    var newValue: number = +value
    if (isNaN(newValue)) {
      return "NA"
    } else {
      return newValue
    }
  }

  addNew(){
    let obj = {
      type : "Add Employee"
    }
    this.service.dataTranster.next(obj)
    this.router.navigateByUrl('operations')
  }

  edit(data: (string | number)) {
    console.log("Index :", data);
    data['type'] = "Edit Employee"
    this.service.editData.next(data);
    this.router.navigateByUrl('operations')
  }

}
