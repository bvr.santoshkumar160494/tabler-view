import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  constructor() { }

  public dataTranster = new BehaviorSubject({})
  public editData = new BehaviorSubject({})
  public formdata = new BehaviorSubject({})
}
